import matplotlib.pyplot as plt 
import numpy as num
 
x_coordinates = [1, 2]
y_coordinates = [3, 3]
 
plt.plot(x_coordinates, y_coordinates)
 
plt.show()
 
num_segmentos = 20
rad = 2
cx = 2
cy = 0
 
angulo = num.linspace(0, 2*num.pi, num_segmentos+1)
x = rad * num.cos(angulo) + cx
y = rad * num.sin(angulo) + cy
 
plt.plot(x, y, color="red", markersize=1)
plt.plot(x, y, 'bo')
 
plt.title("Circulos")
plt.xlabel("X")
plt.ylabel("Y")
plt.gca().set_aspect('equal')
plt.grid()
plt.show()
